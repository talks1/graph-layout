# graph-layout Library

## About

A library with algorithms which lay out hierarchical graphs of nodes and links

### Types of layouts

- Sankey
- Circular Sankey
- Hierarchical Sankey

## Install

```
npm install graph-layout
yarn install graph-layout
```

## How to use

Graphs will have the structure of
```
{
    nodes: [<node>,...],
    links: [<link>,...]
}
```

Nodes of the structure of
```
{ 
    id: <id>
    parent: <parentId>
}
```

Links have the structure of
```
{ 
    source: <node id>,
    target: <node id>,
    relationship: <type of relationship>
}
```
