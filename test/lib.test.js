import Debug from 'debug'
import { describe } from 'riteway'
import { layout, centerPoint } from '../index.js'
const debug = Debug('test')

const graph1 = {
  nodes: [{
    id: '1', parentId: 'A' // The parents dont exist on purpose for this test
  }, {
    id: '2', parentId: 'B' // The parents dont exist on purpose for this test
  }],
  links: [{
    source: '1', target: '2', relationship: 'association'
  }, {
    source: 'A', target: 'B', relationship: 'association'
  }]
}

const graph2 = {
  nodes: [{
    id: '1', parentId: null
  }, {
    id: '2', parentId: null
  }, {
    id: '1.1', parentId: '1'
  }, {
    id: '2.1', parentId: '2'
  }],
  links: [{
    source: '1.1', target: '2.1', relationship: 'association'
  }]
}

const graph3 = {
  nodes: [{
    id: '1', parentId: null
  }, {
    id: '2', parentId: null
  }, {
    id: '1.1', parentId: '1'
  }, {
    id: '2.1', parentId: '2'
  }, {
    id: '1.1.1', parentId: '1.1'
  }, {
    id: '2.1.1', parentId: '2.1'
  }],
  links: [{
    source: '1.1.1', target: '2.1.1', relationship: 'association'
  }]
}

const graph4 = {
  nodes: [{ id: '1', parentId: null }, { id: '2', parentId: null },
  { id: '1.1', parentId: '1' }, { id: '1.2', parentId: '1' },
  { id: '2.1', parentId: '2' }, { id: '2.2', parentId: '2' }
],
  links: [
    { source: '1.1', target: '1.2', relationship: 'association' },
    { source: '2.1', target: '2.2', relationship: 'association' },
    { source: '1.2', target: '2.1', relationship: 'association' }
  ]
}

const graph5 = {
  nodes: [{ id: '1', parentId: null }, { id: '2', parentId: null },
  { id: '1.1', parentId: '1' }, { id: '1.2', parentId: '1' },
  { id: '2.1', parentId: '2' }, { id: '2.2', parentId: '2' },
  { id: '1.1.1', parentId: '1.1' }, { id: '1.1.2', parentId: '1.1' },
  { id: '1.2.1', parentId: '1.2' }, { id: '1.2.2', parentId: '1.2' },
  { id: '2.1.1', parentId: '2.1' }, { id: '2.1.2', parentId: '2.1' },
  { id: '2.2.1', parentId: '2.2' }, { id: '2.2.2', parentId: '2.2' }
],
  links: [
    { source: '1.1.1', target: '1.1.2', relationship: 'association' },
    { source: '1.2.1', target: '1.2.2', relationship: 'association' },

    { source: '1.1.2', target: '1.2.1', relationship: 'association' },

    { source: '2.1.1', target: '2.1.2', relationship: 'association' },
    { source: '2.2.1', target: '2.2.2', relationship: 'association' },

    { source: '2.1.2', target: '2.2.1', relationship: 'association' },

    { source: '1.2.1', target: '2.1.1', relationship: 'association' }
  ]
}

const graph_loop = {
  nodes: [{
    id: '1', parentId: null
  }, {
    id: '2', parentId: null
  }, {
    id: '3', parentId: null
  }],
  links: [{
    source: '1', target: '2', relationship: 'association'
  }, {
    source: '2', target: '3', relationship: 'association'
  }, {
    source: '3', target: '1', relationship: 'association'
  }]
}

const graph_linked_children = {
  nodes: [{
    id: '1', parentId: null
  }, {
    id: '1.1', parentId: '1'
  }, {
    id: '2.1', parentId: '1'
  },
  {
    id: '2.1.1', parentId: '2.1'
  }, {
    id: '2.1.2', parentId: '2.1'
  }],
  links: [
    {source: '2.1.1', target: '2.1.2', relationship: 'association'}
  ]
}

describe('circular - 2 nodes with a link', async (assert) => {
  let newGraph = layout({
    graph: graph1,
    dimensions: [125, 100],
    layoutType: 'circular-sankey'
  })
  let nodeCenters = graph1.nodes
  .map(nd => centerPoint(newGraph.nodes.find(n => n.id === nd.id)))
  .join(' ')
  assert({
    given: '2 nodes and a link',
    should: 'have nodes positioned',
    actual: nodeCenters,
    expected: '25,50 100,50'
  })
})

describe.only('hierachical - 2 nodes with a link', async (assert) => {
  try {
    let scenarios = [
      {        
        given: '3 nodes in a loop',
        graph: graph_loop,
        nodelist: [],
        expect: ` 3`
      },
      {
        //enabled: true,
        given: '2 nodes and no links',
        graph: { nodes: graph2.nodes, links: [] },
        nodelist: ['1', '2'],
        expect: `25,50 75,50 0`
      },
      {
        
        given: '2 nodes and a link',
        graph: graph1,
        nodelist: ['1', '2'],
        expect: `20,50 80,50 1`
      },
      {
        given: '2 and link at lowest level',
        graph: graph2,
        nodelist: ['1.1', '2.1'],
        expect: `20,50 80,50 2`
      },
      {
        given: '3 levels with link at lowest level',
        graph: graph3,
        nodelist: ['1.1.1', '2.1.1'],
        expect: `20,50 80,50 3`
      },
      {
        given: '2 levels with links at lowest level',
        graph: graph4,
        nodelist: ['1', '2'],
        expect: `20,50 80,50 4`
      },
      {
        given: '3 levels with links at lowest level',
        graph: graph5,
        nodelist: ['1', '2'],
        expect: `20,50 80,50 11`
      },
      {
        //enabled: true,
        given: '2 with child with child with graph',
        graph: graph_linked_children,
        nodelist: ['1','1.1','2.1','2.1.1','2.1.2'],
        expect: `50,50 25.5,50 74.5,50 61.6,50 87.4,50 1`
      },
    ]

    let enabled = scenarios.filter(s => s.enabled)
    scenarios = enabled.length ? enabled : scenarios

    for (let i = 0; i < scenarios.length; i++) {
      const scenario = scenarios[i]
      let newGraph = layout({
        graph: scenario.graph,
        dimensions: [100, 100],
        offset: [10, 10],
        layoutType: 'hierarchical-sankey'
      })

      newGraph.nodes.forEach(n => {
        console.log(`${n.id} [${n.x0} ${n.y0}] [${n.x1} ${n.y1}] ${n.level} `)
      })
      newGraph.links.forEach(l => {
        console.log(`${l.path}`)
      })

      let nodeCenters = scenario.nodelist
        .map(nId => centerPoint(newGraph.nodes.find(n => n.id === nId)))
        .join(' ')

        assert({
          given: '2 nodes and a link',
          should: 'have nodes positioned',
          actual: `${nodeCenters} ${newGraph.links.length}`,
          expected: scenario.expect
        })
    }
    } catch (ex) {
      console.log(ex)
    }
})
