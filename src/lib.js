import Debug from 'debug'

import hierarchicalSankey from './hierarchical-sankey'
import circularSankey from './circular-sankey'

const debug = Debug('lib-graph-layout')

export { centerPoint } from './common'

export function layout (options) {
  let { graph, offset = [0, 0], layoutType } = options

  let result = {
    nodes: graph.nodes.map(n => ({ ...n })),
    links: graph.links.map(l => ({ ...l }))
  }

  switch (layoutType) {
    case 'circular-sankey': {
      graph = circularSankey({ ...options, graph: result })
      break
    }
    default: {
      graph = hierarchicalSankey({ ...options, graph: result })
      break
    }
  }

  return graph
}
