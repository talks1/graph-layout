import { layoutAsFlow } from './sankey-layout'

export default function layout (options) {
    let { graph, dimensions, offset, nodeWidth, nodeHeight, nodeRatio, nodePadding, layoutType = 'center' } = options
    graph.links = graph.links.map(l => ({ ...l, value: l.value ? l.value : 10 }))

    graph.links = graph.links.filter(l => {
        let s = (typeof l.source !== 'object') ? graph.nodes.find(n => n.id === l.source) : l.source
        let t = (typeof l.target !== 'object') ? graph.nodes.find(n => n.id === l.target) : l.target
        return s && t
    })

    graph = layoutAsFlow({ graph: graph, dimensions, offset, nodeWidth, nodeHeight, nodeRatio, nodePadding, layoutType })
    return graph
}
