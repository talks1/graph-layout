import { sankeyLeft, sankeyJustify, sankeyCenter } from './align'
import sankeyCircular from './sankeyCircular'
import { max } from 'd3-array'
import { scaleLinear } from 'd3-scale'

// determines the number of rows/columns in the laid out graph
export function deriveRowsColumns (nodes) {
    let columnsMap = nodes.reduce((h, n) => {
        if (!h[n.x0]) { h[n.x0] = [n] } else { h[n.x0].push(n) }
        return h
    }, {})
    let columns = Object.keys(columnsMap).length
    let rows = max(Object.values(columnsMap), d => d.length)
    let result = {
        columns,
        rows
    }
    return result
}

function deriveSizeFromLayout (nodes, nodeWidth, nodeHeight) {
    let { rows, columns } = deriveRowsColumns(nodes)
    let result = {
        rows,
        columns,
        width: columns * nodeWidth + (columns - 1) * nodeWidth / 2,
        height: rows * nodeHeight + (rows - 1) * nodeHeight / 2
    }
    return result
}

export function layoutAsFlow (options) {
    const { graph, dimensions, offset = [0, 0], nodeWidth = 10, nodeHeight = 30, nodeRatio = 1, nodePadding = 10, layoutType = sankeyJustify } = options

    let sankeyFactory = sankeyCircular()
        .nodeWidth(nodeWidth)
        .nodePadding(nodePadding)
        .nodePaddingRatio(nodeRatio)
        .offset([0, 0])
        .size([100, 100])
        .nodeId(d => d.id)
        .nodeAlign(sankeyLeft)
        .iterations(32)

    let sankeyData = sankeyFactory(graph)

    let dims = deriveSizeFromLayout(sankeyData.nodes, nodeWidth, nodeHeight)

    // let dwidth = dims.width > dimensions[0] ? dims.width : dimensions[0]
    // let dheight = dims.height > dimensions[1] ? dims.height: dimensions[1]
    // let dheight = dims.height

    let ndWidth = dimensions[0] / (dims.columns + (dims.columns - 1) * 0.5)
    let ndPadding = ndWidth / 2

    sankeyFactory = sankeyCircular()
        .nodeWidth(ndWidth)
        .nodePadding(ndPadding)
        // .nodePaddingRatio(nodeRatio)
        .size(dimensions)
        .offset(offset)
        .nodeId(d => d.id)
        .nodeAlign(sankeyCenter)
        .iterations(32)
        // .nodeValue((n)=>{
        //     return n.cost ? n.cost : 0
        //     //return 100
        // })

    sankeyData = sankeyFactory(graph)

    // let xScale = scaleLinear().domain([0, dims.width]).range([0, dimensions[0]])
    // let yScale = scaleLinear().domain([0, dims.height]).range([0, dimensions[1]])

    // graph.nodes.forEach(n => {
    //     n.x0 = xScale(n.x0)
    //     n.x1 = xScale(n.x1)
    //     n.y0 = yScale(n.y0)
    //     n.y1 = yScale(n.y1)
    // })

    graph.nodes.forEach(n => {
        n.nodeWidth = ndWidth
    })

    let sankeyNodes = sankeyData.nodes
    let sankeyLinks = sankeyData.links

    return {
        dimensions: dims,
        nodes: sankeyNodes,
        links: sankeyLinks
    }
}
