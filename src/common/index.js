import { map } from 'd3-collection'
import svgpath from 'svgpath'

// returns a function, using the parameter given to the sankey setting
export function constant (x) {
    return function () {
      return x
    }
  }

// Return the node from the collection that matches the provided ID, or throw an error if no match
export function find (nodeById, id) {
    var node = nodeById.get(id)
    // if (!node) throw new Error('missing: ' + id)
    return node
}

export function centerPoint (node) {
    return [node.x0 + (node.x1 - node.x0) / 2, node.y0 + (node.y1 - node.y0) / 2]
  }

export const offsetNode = (offset) => n => {
    n.x0 = n.x0 + offset[0]
    n.x1 = n.x1 + offset[0]
    n.y0 = n.y0 + offset[1]
    n.y1 = n.y1 + offset[1]
    return n
}

export const offsetLink = (offset) => l => {
    if (l.path) { l.path = svgpath(l.path).translate(offset[0], offset[1]).toString() }
    return l
}

// Populate the sourceLinks and targetLinks for each node.
// Also, if the source and target are not objects, assume they are indices.
export function computeNodeLinks (graph, options = {}) {
    let { id = n => n.id } = options

    graph.nodes.forEach(function (node, i) {
        node.index = i
        node.sourceLinks = []
        node.targetLinks = []
    })
    var nodeById = map(graph.nodes, id)
    let nlinks = graph.links.filter(function (link, i) {
        link.index = i
        var source = link.source
        var target = link.target
        if (typeof source !== 'object') {
            let n = find(nodeById, source)
            if (!n) { return null }
            source = link.source = n
        }
        if (typeof target !== 'object') {
            let n = find(nodeById, target)
            if (!n) { return null }
            target = link.target = n
        }
        source.sourceLinks.push(link)
        target.targetLinks.push(link)
        return link
    })
    graph.links = nlinks
    return graph
    }

// Populate level attribute base on depth with respect to parentChildProjection
export function computeNodeLevels (graph, options = {}) {
    let maxLevel = 0
    let { id = n => n.id, parent = n => n.parentId } = options
    graph.nodes = graph.nodes.map(n => ({ ...n, level: 1 }))
    var nodeById = map(graph.nodes, id)
    let changes = false
    do {
        changes = false
        graph.nodes.forEach(n => {
            let pId = parent(n)
            if (pId) {
                let pNode = graph.nodes.find(n => n.id === pId)
                if (pNode) {
                    const pN = nodeById.get(pId)
                    if (n.level <= pN.level) {
                        n.level = pN.level + 1
                        changes = true
                        if (maxLevel < n.level) { maxLevel = n.level }
                    }
                }
            }
        })
    } while (changes)

    return maxLevel
}

// Compute links at all levels considering links of lower levels
export function computeNodeLevelLinks (graph, options = {}) {
    let { id = n => n.id, parent = n => n.parentId } = options
    let maxLevel = graph.nodes.reduce((h, n) => n.level > h ? n.level : h, 0)
    var nodeById = map(graph.nodes, id)

    let links = graph.links.map(l => ({ ...l, id: `${l.source}-${l.target}` }))
    var linksById = map(links, id)
    for (let l = maxLevel; l >= 0; l--) {
        let levelNodes = graph.nodes.filter(n => n.level === l)
        levelNodes.forEach(sN => {
            let sourceParent = parent(sN)
            if (sourceParent) {
                let sPId = parent(sN)
                let sParentNode = graph.nodes.find(n => n.id === sPId)
                if (sParentNode) {
                    let sourceLinks = Object.values(linksById).filter(l => l.source === sN.id)
                    sourceLinks.forEach(l => {
                        let tN = nodeById.get(l.target)
                        let tPId = parent(tN)
                        let tParentNode = graph.nodes.find(n => n.id === tPId)
                        if (tParentNode) {
                            if (tPId !== sPId) {
                                let linkId = `${sPId}-${tPId}`
                                if (!linksById[linkId]) {
                                    linksById[linkId] = { id: linkId, source: sPId, target: tPId }
                                }
                            }
                        }
                    })
                }
            }
        })
    }
    graph.links = Object.values(linksById)
    return graph
    }

export function findLinksForNodes (links, nodes) {
    return links.filter(l => {
        let source = nodes.find(n => l.source.id === n.id)
        let target = nodes.find(n => l.target.id === n.id)
        return source && target
    })
}
