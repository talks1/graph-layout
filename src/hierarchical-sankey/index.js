import { map } from 'd3-collection'
import { computeNodeLevels, computeNodeLinks, computeNodeLevelLinks, findLinksForNodes, offsetNode, offsetLink } from '../common'
import sankeyCircular from '../circular-sankey'
import { linkHorizontal } from 'd3-shape'

export default function layout (options) {
    let graph = layoutAsFlow(options)
    return graph
}

function layoutAsFlow (options) {
    let { graph, dimensions, offset, padding = 2 } = options

    // 1.  Add level prop to all nodes
    computeNodeLevels(graph)

    // 2.  Add links at each level
    computeNodeLevelLinks(graph)

    // 3.  Associate the nodes with their respective links, and vice versa
    computeNodeLinks(graph)

    layoutLevels(graph, dimensions, offset, padding, {})

    // Set cx,cy for nodes
    setCenterPoint(graph)

    // set link start end points
    positionLinks(graph)

    return graph
}

function setCenterPoint (graph) {
    graph.nodes.forEach(n => {
        n.cx = n.x0 + (n.x1 - n.x0) / 2
        n.cy = n.y0 + (n.y1 - n.y0) / 2
    })
}

function positionLinks (graph) {
    graph.links = graph.links.map(l => {
        return { ...l, x0: l.source.cx, x1: l.target.cx, y0: l.source.cy, y1: l.target.cy }
    })
}

function layoutLevels (graph, dimensions, offset, padding, options = {}) {
    let { id = n => n.id, parent = n => n.parentId } = options
    let rnodes = []
    let links = graph.links.map(l => ({ ...l, id: `${l.source.id}-${l.target.id}` }))
    var linksById = map(links, l => l.id)

    let maxLevel = graph.nodes.reduce((h, n) => n.level > h ? n.level : h, 0)
    for (let l = 1; l <= maxLevel; l++) {
        let parentNodes = graph.nodes.filter(n => n.level === l - 1)
        if (parentNodes.length === 0) {
            let groupNodes = graph.nodes.filter(n => n.level === l)
            let groupLinks = findLinksForNodes(graph.links, groupNodes)
            if (groupLinks.length !== 0) {
                groupLinks = groupLinks.filter(l=>groupNodes.find(n=>n.id===l.source.id) && groupNodes.find(n=>n.id===l.target.id))
                groupNodes = groupNodes.filter(n=>groupLinks.find(l=>l.source.id===n.id || l.target.id===n.id))
                let groupGraph = sankeyCircular({ graph: { nodes: groupNodes, links: groupLinks }, dimensions })
                rnodes = rnodes.concat(groupGraph.nodes)
                groupGraph.links.forEach(link => linksById.set(link.id, link))
            } else {
                groupNodes.forEach((n, i) => {
                    n.x0 = dimensions[0] * i / groupNodes.length + padding / 2
                    n.x1 = dimensions[0] * (i + 1) / groupNodes.length - padding / 2
                    n.y0 = 0 + padding / 2
                    n.y1 = dimensions[1] - padding / 2
                })
                rnodes = rnodes.concat(groupNodes)
            }
        } else {
            parentNodes.forEach(pN => {
                let groupNodes = graph.nodes.filter(n => n.level === l && parent(n) === id(pN))
                let groupLinks = findLinksForNodes(graph.links, groupNodes)
                if (groupLinks.length === 0) {
                    let dims = [pN.x1 - pN.x0, pN.y1 - pN.y0]
                    groupNodes.forEach((n, i) => {
                        n.x0 = pN.x0 + dims[0] * i / groupNodes.length + padding / 2
                        n.x1 = pN.x0 + dims[0] * (i + 1) / groupNodes.length - padding / 2
                        n.y0 = pN.y0 + 0 + padding / 2
                        n.y1 = pN.y0 + dims[1] - padding / 2
                    })
                    rnodes = rnodes.concat(groupNodes)
                } else if (groupNodes.length > 1 && groupLinks.length > 0) {    
                    let offset = [pN.x0 + padding, pN.y0 + padding]
                    groupLinks = groupLinks.filter(l=>groupNodes.find(n=>n.id===l.source.id) && groupNodes.find(n=>n.id===l.target.id))
                    groupNodes = groupNodes.filter(n=>groupLinks.find(l=>l.source.id===n.id || l.target.id===n.id))
                    console.log(groupNodes)
                    console.log(groupLinks)
                    console.log([pN.x1 - pN.x0 - 2 * padding, pN.y1 - pN.y0 - 2 * padding])
                    console.log(offset)
                    let groupGraph = sankeyCircular({ graph: { nodes: groupNodes, links: groupLinks },
                        dimensions: [pN.x1 - pN.x0 - 2 * padding, pN.y1 - pN.y0 - 2 * padding],
                     })
                    let offsetNodeTransform = offsetNode(offset)
                    let offsetLinkTransform = offsetLink(offset)
                    groupGraph.nodes = groupGraph.nodes.map(offsetNodeTransform)
                    groupGraph.links = groupGraph.links.map(offsetLinkTransform)

                    rnodes = rnodes.concat(groupGraph.nodes)
                    groupGraph.links.forEach(link => linksById.set(link.id, link))
                }
            })
        }
    }
    graph.nodes = rnodes
    graph.links = Object.values(linksById)
    graph.links.forEach(link => {
        if (!link.path) {
            var normalPath = linkHorizontal()
            .source(function (d) {
              var x = d.source.x0 + (d.source.x1 - d.source.x0)
              var y = d.y0
              return [x, y]
            })
            .target(function (d) {
              var x = d.target.x0
              var y = d.y1
              return [x, y]
            })
          link.path = normalPath(link)
        }
    })
}
